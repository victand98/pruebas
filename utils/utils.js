function saveNumbers(input, separator) {
    var numbers = [];
    var data = input.replace(/\s\s+/g, ' ');
    data = data.split(separator);
    for (var i = 0; i < data.length; i++) {
        numbers.push(parseFloat(data[i]))
    }
    return numbers;
}

function drawTable(table, data) {
    var tbody = table.find('tbody')
    var html = ``;
    data.forEach(element => {
        html += `<tr><td>${element}</td></tr>`
    });
    tbody.html(html);
}

function redondear(number, decimals) {
    var places = Math.pow(10, decimals);
    return Math.round(number * places) / places;
}

function factorial(n) {
    var total = 1;
    for (i = 1; i <= n; i++) {
        total = total * i;
    }
    return total;
}