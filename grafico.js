new Morris.Line({
    // ID of the element in which to draw the chart.
    element: 'myfirstchart',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [
        { x: 0.1, y: 0, xn: 0.2 },
        { x: 0.122, y: 0, xn: 0.4 },
        { x: 0.2, y: 0, xn: 0.6  },
        { x: 0.3, y: 0, xn: 0.8  },
        { x: 1, y: 0, xn: 1  }
    ],
    // The name of the data record attribute that contains x-values.
    ykeys: ['y', 'xn'],
    // A list of names of data record attributes that contain y-values.
    xkey: 'x',
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: '',
    parseTime: false,
});