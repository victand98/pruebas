$.validator.setDefaults({
    submitHandler: function () {
        var m = parseInt($('#m').val()),
            a = parseInt($('#a').val()),
            c = parseInt($('#c').val()),
            xn = parseInt($('#xn').val());
        $('#formula').text(`(${a}(${xn}) + ${c})/${m}`)
        generate_aleatory(m, a, c, xn);
    }
});

$().ready(function () {

    $("#form").validate({
        rules: {
            m: {
                modulo_mayor: false,
                is_prime: false,
            },
            a: {
                is_impar: true,
                no_divisible_3_5: false,
                complete_period: false
            },
            c: {
                is_impar: false,
                is_relative_prime: false
            }
        }
    });
});

$.validator.addMethod("modulo_mayor", function (value, element) {
    return this.optional(element) || modulo_mayor(parseInt(value));
}, "El modulo debe ser mayor que: Xn, c y a.");

$.validator.addMethod("is_prime", function (value, element) {
    return this.optional(element) || is_prime(parseInt(value));
}, "El valor debe ser un número primo.");

$.validator.addMethod("is_impar", function (value, element) {
    return this.optional(element) || is_impar(parseInt(value));
}, "El valor debe ser un número impar.");

$.validator.addMethod("no_divisible_3_5", function (value, element) {
    return this.optional(element) || no_divisible_3_5(parseInt(value));
}, "El valor no debe ser divisible para 3 ó 5.");

$.validator.addMethod("complete_period", function (value, element) {
    return this.optional(element) || complete_period(parseInt(value));
}, "El valor no puede generar un ciclo o periodo completo.");

$.validator.addMethod("is_relative_prime", function (value, element) {
    return this.optional(element) || is_relative_prime(parseInt(value));
}, "El valor no es relativamente primo a m.");

function modulo_mayor(m) {
    var a = $('#a').val();
    var c = $('#c').val();
    var xn = $('#xn').val();
    if (m > a && m > c && m > xn) {
        return true;
    }
    return false;

}

function is_prime(num) {
    for (var i = 2; i < num; i++)
        if (num % i === 0) return false;
    return num > 1;
}

function is_impar(value) {
    if (value % 2 == 0)
        return false
    return true;
}

function no_divisible_3_5(value) {
    if (value % 3 == 0 || value % 5 == 0)
        return false
    return true;
}

function complete_period(value) {
    var m = $('#m').val();
    if (m % 4 == 0) {
        if ((value - 1) % 4 == 0)
            return true;
    }
    var factors_primes_m = get_factors_primes(m);
    var result = factors_primes_m.filter(function (b) {
        return (value - 1) % b == 0;
    });
    if (result.length > 0)
        return true;

    return false;
}

function get_factors(value) {
    var factors = [1, parseInt(value)];
    if (value > 1) {
        var cont = 2;
        var limit = value;
        while (cont < limit) {
            if (value % cont == 0) {
                factors.push(cont);
                if (cont != value / cont)
                    factors.push(value / cont);
                limit = value / cont;
            }
            cont++;
        }
    }
    return factors;
}

function get_factors_primes(value) {
    var factors = [];
    if (value > 2) {
        var cont = 2;
        var limit = value;
        while (cont < limit) {
            if (value % cont == 0) {
                if (is_prime(cont) && is_prime(value / cont)) {
                    if (cont == value / cont) {
                        factors.push(cont);
                    } else
                        factors.push(cont, value / cont);
                }
                limit = value / cont;
            }
            cont++;
        }
    }
    return factors;
}

function is_relative_prime(value) {
    var m = $('#m').val();
    var value_factors = get_factors(value);
    var m_factors = get_factors(m);
    var result = value_factors.filter(function (e) {
        return m_factors.indexOf(e) > -1;
    });
    if (result.length == 1)
        return true;
    return false;
}

function generate_aleatory(m, a, c, xn) {
    var s = xn,
        x = s;

    var aleatory = [];
    var table = $('#table tbody');
    var html = '';
    var cont = 0;

    while (!aleatory.includes(s)) {
        var z = ((a * x) + c) % m;
        var aux = (a * x + c) - z;
        aux = aux / m;
        x = z;
        aleatory.push(x);
        html += drawTable(cont == 0 ? s : aleatory[cont - 1], cont, z, aux, m);
        cont++;
    }
    var uniformeDiv = $('#numeros-uniformes');
    uniformes(uniformeDiv, aleatory, m);
    table.html(html);
}

function drawTable(xn, cont, z, aux, m) {
    var html = `<tr><td>${cont}</td>`;
    html += `<td>${xn}</td>`;
    html += `<td>${aux} + ${z}/${m}</td>`;
    html += `<td>${z}</td>`;
    html += `<td>${z}/${m}</td></tr>`;
    return html;
}

function uniformes(div, aleatory, m) {
    var html = ``;
    for (let i = 0; i < aleatory.length; i++) {
        var number = aleatory[i] / m;
        html += `${number.toFixed(5)} `
    }
    div.html(html);
}