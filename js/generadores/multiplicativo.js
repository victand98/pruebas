$.validator.setDefaults({
    submitHandler: function () {
        var m = get_m(parseInt($('#d').val())),
            a = get_a(parseInt($('#t').val())),
            xn = parseInt($('#xn').val());
        $('#formula').html(`X<sub>n+1</sub> = ${a}X<sub>n</sub> mod ${m}\n<br><b>Periodo: </b>${m / 4}`)
        generate_aleatory(m, a, xn);
    }
});

$().ready(function () {

    $("#form").validate({
        rules: {
            m: {
                modulo_mayor: true
            },
            xn: {
                is_impar: true,
                is_relative_prime: true
            }
        }
    });

    $('#d').on('focusout keyup', function () {
        var d = $(this).val();
        $('#m').val(get_m(d));
    });

    $('#t').on('focusout keyup', function () {
        var t = $(this).val();
        $('#a').val(get_a(t));
    });
});

$.validator.addMethod("modulo_mayor", function (value, element) {
    return this.optional(element) || modulo_mayor(parseInt(value));
}, "El modulo debe ser mayor que: Xn.");

$.validator.addMethod("is_impar", function (value, element) {
    return this.optional(element) || is_impar(parseInt(value));
}, "El valor debe ser un número impar.");

$.validator.addMethod("is_relative_prime", function (value, element) {
    return this.optional(element) || is_relative_prime(parseInt(value));
}, "El valor no es relativamente primo a m.");

function modulo_mayor(m) {
    var xn = $('#xn').val();
    if (m > xn) {
        return true;
    }
    return false;
}

function get_m(d) {
    return Math.pow(2, d);
}

function get_a(t) {
    var a = (8 * t) + 3;
    return a;
}

function is_impar(value) {
    if (value % 2 == 0)
        return false
    return true;
}

function get_factors(value) {
    var factors = [1, parseInt(value)];
    if (value > 1) {
        var cont = 2;
        var limit = value;
        while (cont < limit) {
            if (value % cont == 0) {
                factors.push(cont);
                if (cont != value / cont)
                    factors.push(value / cont);
                limit = value / cont;
            }
            cont++;
        }
    }
    return factors;
}

function is_relative_prime(value) {
    var m = $('#m').val();
    var value_factors = get_factors(value);
    var m_factors = get_factors(m);
    var result = value_factors.filter(function (e) {
        return m_factors.indexOf(e) > -1;
    });
    if (result.length == 1)
        return true;
    return false;
}

function generate_aleatory(m, a, xn) {
    var s = xn,
        x = s;

    var aleatory = [];
    var table = $('#table tbody');
    var html = '';
    var cont = 0;

    while (!aleatory.includes(s)) {
        var z = ((a * x)) % m;
        x = z;
        aleatory.push(x);
        html += drawTable(cont == 0 ? s : aleatory[cont - 1], cont);
        cont++;
    }

    table.html(html);
}

function drawTable(xn, cont) {
    var html = `<tr><td>${cont}</td>`;
    html += `<td>${xn}</td></tr>`;
    return html;
}