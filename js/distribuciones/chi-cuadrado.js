function average(numbers) {
    var avg = 0;
    for (let i = 0; i < numbers.length; i++) {
        avg += numbers[i];
    }
    avg /= numbers.length;
    return avg;
}

function variance(numbers, average) {
    var vrc = 0;
    for (let i = 0; i < numbers.length; i++) {
        vrc += Math.pow((numbers[i] - average), 2);
    }
    vrc /= (numbers.length - 1);
    return vrc;
}

function generateIntervals(numbers, n) {
    var intervals = {};
    var maxNumber = Math.max(...numbers);
    var minNumber = Math.min(...numbers);
    var m = maxNumber - minNumber;
    var z = m / (n - 2 <= 0 ? 1 : n - 2);
    var limInf = 0;
    var limSup = minNumber;
    for (let i = 0; i < n; i++) {
        if (i == 0) {
            intervals[`[${redondear(limInf, 2)}-${redondear(limSup, 2)}]`] = [];
        } else {
            intervals[`(${redondear(limInf, 2)}-${redondear(limSup, 2)}]`] = [];
        }
        limInf = limSup;
        limSup += z;
    }
    limInf = 0;
    limSup = minNumber;
    for (let i = 0; i < n; i++) {
        var values = numbers.filter(v => v <= limSup && v > limInf);
        if (i == 0) {
            values = numbers.filter(v => v <= limSup && v >= limInf);
            Array.prototype.push.apply(intervals[`[${redondear(limInf, 2)}-${redondear(limSup, 2)}]`], values);
        } else {
            Array.prototype.push.apply(intervals[`(${redondear(limInf, 2)}-${redondear(limSup, 2)}]`], values);
        }
        limInf = limSup;
        limSup += z;
    }
    return intervals;
}

function createHistogram(intervals, chart) {
    var intervalKeys = Object.keys(intervals);
    var intervalValues = Object.values(intervals);
    var options = {
        title: {
            text: "Histograma"
        },
        data: [
            {
                type: "column",
                dataPoints: intervalKeys.map((item, index) => {
                    return { label: item, y: intervalValues[index].length }
                })
            }
        ]
    };
    chart.CanvasJSChart(options);
}

function chiCuadradoFO(intervals) {
    var fo = [];
    var intervalValues = Object.values(intervals);
    for (let i = 0; i < intervalValues.length; i++) {
        fo.push(intervalValues[i].length);
    }
    return fo;
}

function poissonP(numbers, n, average) {
    var px = [];
    var maxNumber = Math.max(...numbers);
    var minNumber = Math.min(...numbers);
    var m = maxNumber - minNumber;
    var z = m / (n - 2 <= 0 ? 1 : n - 2);
    var limInf = 0;
    var limSup = minNumber;
    for (let i = 0; i < n; i++) {
        var p = (Math.pow(average, limInf) * Math.pow(Math.E, -average)) / factorial(limInf);
        p += (Math.pow(average, limSup) * Math.pow(Math.E, -average)) / factorial(limSup);
        px.push(p);
        limInf = limSup;
        limSup += z;
    }
    return px;
}

function chiCuadradoFE(N, px) {
    var fe = [];
    for (let i = 0; i < px.length; i++) {
        var p = px[i] * N;
        fe.push(p);
    }
    return fe;
}


