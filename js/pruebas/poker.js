function convertToArray(number) {
    var decimal = number.toString().split('.')[1];
    var arrayDecimal = decimal.toString().split('');
    for (let i = 0; i < arrayDecimal.length; i++) {
        arrayDecimal[i] = parseInt(arrayDecimal[i]);
    }
    return arrayDecimal;
}

function numbersArray(numbers) {
    var generalArray = [];
    for (let i = 0; i < numbers.length; i++) {
        generalArray.push(convertToArray(numbers[i]));
    }
    return generalArray;
}

function updateArray(numbers, indexes) {
    var array = [];
    for (var i = indexes.length - 1; i >= 0; i--) {
        numbers.splice(indexes[i], 1);
    }
    array = numbers;
    return array;
}

//EXPECTED FRECUENCY
function getPokerFE(n) {
    return {
        'different': n * 0.3024,
        'pair': n * 0.504,
        'twoPair': n * 0.108,
        'third': n * 0.072,
        'full': n * 0.009,
        'poker': n * 0.0045,
        'quintile': n * 0.0001
    };
}

//OBSERVED FRECUENCY
function getPokerFO(different, pair, twoPair, third, full, poker, quintile) {
    return {
        different, pair, twoPair,
        third, full, poker, quintile
    };
}
//Check for ALL possible pairs
function checkPairs(numbers) {
    var result = [];
    numbers.forEach((element, index) => { //0.99887
        for (i = 0; i < element.length - 1; i++) {
            for (j = i + 1; j < element.length; j++) {
                if (element[i] == element[j]) {
                    result.push(index); //[0] [0,0]
                }
            }
        }
    });
    return result;
}

//Function to check for double pairs. Will not check in threes and fours, only strict double pairs
function checkTwoPairs(numbers) {
    var pairs = checkPairs(numbers);
    var sorted_arr = pairs.slice().sort();
    var results = [];
    for (var i = 0; i < sorted_arr.length - 1; i++) {
        if (sorted_arr[i + 1] == sorted_arr[i]) {
            results.push(sorted_arr[i]);
        }
    }
    return results;
}

//Check for ALL possible threes
function checkThird(numbers) {
    var result = [];
    numbers.forEach((element, index) => {
        for (i = 0; i < element.length - 2; i++) {
            for (j = i + 1; j < element.length - 1; j++) {
                if (element[i] == element[j]) {
                    for (k = j + 1; k < element.length; k++) {
                        if (element[j] == element[k]) {
                            result.push(index);
                        }
                    }
                }
            }
        }
    });
    return result;
}

//Checking for full
function checkFull(numbers) {
    var pairs = checkPairs(numbers);
    var result = [];
    for (let i = 0; i < pairs.length - 1; i++) {
        var index = 1;
        for (let j = i + 1; j < pairs.length; j++) {
            if (pairs[i] == pairs[j]) {
                index++;
            }
        }
        if (index == 4) {
            result.push(pairs[i]);
        }
    }
    return result;
}

//Check for four
function checkPoker0rQuintille(numbers, n) {
    var result = [];
    var cont = 0;
    numbers.forEach((element, index) => {
        for (i = 0; i < element.length - 1; i++) {
            var rate = [i];
            for (j = i + 1; j < element.length; j++) {
                if (element[i] == element[j]) {
                    rate.push(j);
                }
            }
            if (rate.length == n) {
                result.push(cont);
            }
        }
        cont++;
    });
    return result;
}

// CHI SQUARE
function chiPoker(fe, fo) {
    var sum = Math.pow((fo.different - fe.different), 2) / fe.different;
    sum += Math.pow((fo.pair - fe.pair), 2) / fe.pair;
    sum += Math.pow((fo.twoPair - fe.twoPair), 2) / fe.twoPair;
    sum += Math.pow((fo.third - fe.third), 2) / fe.third;
    sum += Math.pow((fo.full - fe.full), 2) / fe.full;
    sum += Math.pow((fo.poker - fe.poker), 2) / fe.poker;
    sum += Math.pow((fo.quintile - fe.quintile), 2) / fe.quintile;
    return sum;
}

function pokerData(div, table, fe, fo, chi) {
    var thead = table.find('thead');
    var tbody = table.find('tbody');

    var html = `<tr><th></th><th>Frecuencia Observada</th><th>Frecuencia Esperada</th></tr>`;
    thead.html(html);
    html = `<tr><td><b>Todos Diferentes</b></td><td>${fo.different}</td><td>${redondear(fe.different, 3)}</td></tr>`;
    html += `<tr><td><b>Un Par</b></td><td>${fo.pair}</td><td>${redondear(fe.pair, 3)}</td></tr>`;
    html += `<tr><td><b>Dos Pares</b></td><td>${fo.twoPair}</td><td>${redondear(fe.twoPair, 3)}</td></tr>`;
    html += `<tr><td><b>Tercia</b></td><td>${fo.third}</td><td>${redondear(fe.third, 3)}</td></tr>`;
    html += `<tr><td><b>Full</b></td><td>${fo.full}</td><td>${redondear(fe.full, 3)}</td></tr>`;
    html += `<tr><td><b>Poker</b></td><td>${fo.poker}</td><td>${redondear(fe.poker, 3)}</td></tr>`;
    html += `<tr><td><b>Quintilla</b></td><td>${fo.quintile}</td><td>${redondear(fe.quintile, 3)}</td></tr>`;
    tbody.html(html);

    html = `<p><b>X<sup>2</sup>: </b><span>${redondear(chi, 2)}</span>`;
    div.html(html);
}