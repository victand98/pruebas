function convertToBinary(numbers) {
    var binary = [];//array with binaries
    for (var i = 0; i < numbers.length; i++) {
        if (numbers[i] < 0.5) {
            binary.push(0);
        } else {
            binary.push(1);
        }
    }
    return binary;
}

function getR(numbers) {
    var R = 1;
    for (let i = 0; i < numbers.length - 1; i++) {
        if (numbers[i + 1] != numbers[i]) {
            R++;
        }
    }
    return R;
}

function getN1(numbers) {
    var n1 = 0;//how many 0 there are in the total of the array
    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] == 0) {
            n1++;
        }
    }
    return n1;
}

function getN2(numbers) {
    var n2 = 0;//how many 1 there are in the total of the array
    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] == 1) {
            n2++;
        }
    }
    return n2;
}

function averageRandom(numbers) {
    var n1 = getN1(numbers);
    var n2 = getN2(numbers);
    var Xr = ((2 * n1 * n2) / (n1 + n2)) + 1;
    Xr = redondear(Xr, 2);
    return Xr;
}

function standartDeviation(numbers) {
    var n1 = getN1(numbers);
    var n2 = getN2(numbers);
    var par = 2 * n1 * n2;
    var num = par * (par - n1 - n2);
    var den = (Math.pow((n1 + n2), 2) * (n1 + n2 - 1))
    var Sr = Math.sqrt((num / den));
    return Sr;
}

function getH(numbers) {
    var h = 0;
    var R = getR(numbers);
    var average = averageRandom(numbers);
    if (R < average) {
        h = 0.5
    } else {
        h = -0.5
    }
    return h;
}

function zCritic(numbers) {
    var R = getR(numbers), h = getH(numbers), Xr = averageRandom(numbers),
        Sr = standartDeviation(numbers);
    var Z = (R + h - Xr) / Sr;
    return Z
}

function randomData(div, numbers) {
    var Z = zCritic(numbers);
    var zc = 1.96;
    var html = '<p>';
    if (Math.abs(Z) <= zc) {
        //Distribución uniforme
        html += `|${redondear(Z, 2)}| <= 1.96, entonces pertenece a una distribucion uniforme</p>`;
    } else {
        html += `|${redondear(Z, 2)}| > 1.96, entonces no pertenece a una distribucion uniforme</p>`;
    }
    div.html(html);
}